<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BreakdownSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Breakdowns';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="breakdown-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
		<?php if (\Yii::$app->user->can('createBreakdown', ['user' =>$model]) ){ ?>
        <?= Html::a('Create Breakdown', ['create'], ['class' => 'btn btn-success']) ?>
		 <?php } ?>
	 
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
         
			//'level'
			[
				'attribute' => 'level',
				'label' => 'Level',
				'value' => function($model){
							return $model->levelItem->level_name;
					},	
				
			],
			
            //'status'
			[
				'attribute' => 'status',
				'label' => 'Status',
				'value' => function($model){
							return $model->statusItem->status_name;
					},	
				'filter'=>Html::dropDownList('BreakdownSearch[status]', 
				$status, $statuses, ['class'=>'form-control']),					
			],
			
            ['class' => 'yii\grid\ActionColumn'],
			
        ],
    ]); ?>
</div>
