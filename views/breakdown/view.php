<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Breakdown */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Breakdowns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="breakdown-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
		<?php if (\Yii::$app->user->can('updateBreakdown', ['user' =>$model]) ){ ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		 <?php } ?>
	 
		<?php if (\Yii::$app->user->can('deleteBreakdown', ['user' =>$model]) ){ ?>
		<?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
			 <?php } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            //'level'
			[
				
				'label' => 'Level',
				'value' => function($model){
							return $model->levelItem->level_name;
					},	
						
			],
			
            //'status'
			[
				
				'label' => 'Status',
				'value' => function($model){
							return $model->statusItem->status_name;
					},	
						
			],
        ],
    ]) ?>

</div>
